package be.c4j.sot;

public class ProcessInfo {
    private final String version;
    private final long processId;

    public ProcessInfo(final String version,
        final long processId) {
        this.version = version;
        this.processId = processId;
    }

    public String getVersion() {
        return version;
    }

    public long getProcessId() {
        return processId;
    }
}