package be.bewire.reverser;

public class Reverser {

    public String reverse(String stringGoPretify)  {
        return new StringBuilder(stringGoPretify).reverse().toString();
    }
}
