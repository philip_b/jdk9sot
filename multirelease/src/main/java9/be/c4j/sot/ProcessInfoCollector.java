package be.c4j.sot;


public class ProcessInfoCollector {

    public ProcessInfo getInfo() {
        return new ProcessInfo("Java 9",ProcessHandle.current().pid());
    }
}
