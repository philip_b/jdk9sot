package be.c4j.sot;

import java.lang.management.ManagementFactory;

public class ProcessInfoCollector {

    public ProcessInfo getInfo() {

        String jvmName = ManagementFactory.getRuntimeMXBean().getName();
        final int index = jvmName.indexOf('@');
        if (index < 1)
            return new ProcessInfo("Pre Java 9",0); // No PID.
        try
        {
            return new ProcessInfo("Pre Java 9", Long.parseLong(jvmName.substring(0, index)));
        }
        catch (NumberFormatException nfe)
        {
            return new ProcessInfo("Pre Java 9",0);
        }
    }
}
