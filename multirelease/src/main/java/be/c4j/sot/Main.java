package be.c4j.sot;

public class Main {

    public static void main(String[] args) {
        ProcessInfo info = new ProcessInfoCollector().getInfo();

        System.out.println("Java Version: " + info.getVersion());
        System.out.println("PID: " + info.getProcessId() );
    }
}
