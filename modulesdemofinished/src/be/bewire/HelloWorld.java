package be.bewire;

import be.bewire.reverser.Reverser;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class HelloWorld {

    public static void main(String[] args){
        ObjectMapper mapper = new ObjectMapper();

        try {
            Employee employee = mapper.readValue("{\"firstName\":\"Berten\",\"lastName\":\"De Schutter\"}", Employee.class);

            System.out.println(new Reverser().reverse(employee.getLastName()));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
