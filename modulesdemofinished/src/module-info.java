module modulesdemofinished {
    requires reverserfinished;
    requires java.base;
    requires jackson.core;
    requires jackson.databind;
    opens be.bewire to jackson.databind;
}