package be.c4j.sot.collections;

import org.junit.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class StreamApiDemo {

    @Test
    public void forLoop_TakeWhile() throws Exception {
        final ValueHolder a = new ValueHolder(0);

        Stream.iterate(0,i -> i + 1).takeWhile( i -> i < 10).forEach(a::add);

        assertThat(a.getValue()).isEqualTo(45);
    }


    @Test
    public void forLoop_EnkelEvenGetallen() throws Exception {
        final ValueHolder a = new ValueHolder(0);

        Stream.iterate(0, i -> i < 10, i -> i + 1).filter(i -> i % 2 == 0).forEach(a::add);

        assertThat(a.getValue()).isEqualTo(20);
    }

    @Test
    public void forLoop_NegeerDeEerste5Getallen() throws Exception {
        ValueHolder a = new ValueHolder(0);
        Stream<Integer> bestaandeStream = Stream.iterate(0, i -> i < 10, i -> i + 1);
        bestaandeStream.dropWhile(i -> i <5).forEach(a::add);

        assertThat(a.getValue()).isEqualTo(35);
    }


    @Test
    public void nullableStream() throws Exception {
        StreamHolder h = apiCallThatReturnsNull();

        Stream<String> versies = Stream.ofNullable(h).flatMap(StreamHolder::getVersies);

        assertThat(versies.collect(Collectors.toList())).isEmpty();

        StreamHolder withValues = new StreamHolder("Java 7","Java 8", "Java 9");
        Stream<String> bestaandeVersies = Stream.ofNullable(withValues).flatMap(StreamHolder::getVersies);
        assertThat(bestaandeVersies.collect(Collectors.toList())).contains("Java 7","Java 8", "Java 9");

    }

    private StreamHolder apiCallThatReturnsNull() {
        return null;
    }

    private class StreamHolder {
        private Stream<String> versies;

        public StreamHolder(String ... versies) {
            this.versies = Stream.of(versies);
        }

        public Stream<String> getVersies() {
            return versies;
        }
    }

    private class ValueHolder {
        private int value;

        public ValueHolder(int value) {
            this.value = value;
        }


        public void add(Integer toAdd) {
            value = value + toAdd;
        }

        public int getValue() {
            return value;
        }
    }
}
