package be.c4j.sot.collections;

import org.assertj.core.data.MapEntry;
import org.junit.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

public class CollectionsDemo {

    @Test
    public void arrrayList_Manual() throws Exception {
        // Ook zeker vermelden dat Arrays.asList wel bestaat, maar dat dit een slechte implementatie is. Dit is eigenlijk een Wrapper rond een array. Je kan aan de array geen waardes meer toevoegen etc.

        List<String> values = List.of("Java 7","Java 8","Java 9");
        assertThat(values).contains("Java 7","Java 8","Java 9");
    }

    @Test
    public void set_Manual() throws Exception {
        Set<String> values = Set.of("Java 7","Java 7","Java 8","Java 9");
        assertThat(values).contains("Java 7","Java 8","Java 9").hasSize(3);
    }

    @Test
    public void map_Manual() throws Exception {
        Map<String,Integer> versions = Map.of("Java 7",2011,"Java 8",2014,"Java 9",2017);

        assertThat(versions).contains(entry("Java 7",2011),entry("Java 8",2014),entry("Java 9",2017));
    }

    @Test
    public void map_TweedeOptie() throws Exception {
        Map<String,Integer> versions = Map.ofEntries(MapEntry.entry("Java 7",2011),MapEntry.entry("Java 8",2014),MapEntry.entry("Java 9",2017));


        assertThat(versions).contains(entry("Java 7",2011),entry("Java 8",2014),entry("Java 9",2017));
    }
}
