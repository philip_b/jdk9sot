package be.c4j.sot.process;

import org.junit.Test;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Comparator;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;


public class ProcessHandlerDemo {

    @Test
    public void findSpecificProcess() throws IOException {

        //Search IDEA process
        Optional<ProcessHandle> intelliJHandler = ProcessHandle.allProcesses()
            .filter(ph -> ph.info().command().isPresent())
            .filter(ph -> ph.info().command().get().contains("IntelliJ"))
            .findFirst();

        if (intelliJHandler.isPresent()) {
            ProcessHandle intelliJ = intelliJHandler.get();

            System.out.println("PROCESS INFORMATION");
            System.out.println("===================");
            System.out.printf("Process id: %d%n", intelliJ.pid());
            ProcessHandle.Info ideaInfo = intelliJ.info();
            System.out.printf("Command: %s%n", ideaInfo.command().orElse(""));
            //Process Arguments
            String[] args = ideaInfo.arguments().orElse(new String[] {});
            System.out.println("ARGUMENTS:");
            System.out.println("=========");
            for (String arg : args)
                System.out.printf("   %s%n", arg);

            System.out.printf("Command line: %s%n",
                ideaInfo.commandLine().orElse(""));

            System.out.printf("Start time: %s%n",
                ideaInfo.startInstant().orElse(Instant.now()).toString());

            System.out.printf("Run time duration: %sms%n",
                ideaInfo.totalCpuDuration().orElse(Duration.ofMillis(0))
                    .toMillis());

            System.out.printf("Owner: %s%n", ideaInfo.user().orElse(""));

        }

    }

    @Test
    public void currentProcess() throws IOException {
        ProcessHandle processHandle = ProcessHandle.current();

        System.out.println("PROCESS INFORMATION");
        System.out.println("===================");
        System.out.printf("Process id: %d%n", processHandle.pid());

        ProcessHandle.Info processInfo = processHandle.info();

        System.out.printf("Command: %s%n", processInfo.command().orElse(""));
        //Process Arguments
        String[] args = processInfo.arguments().orElse(new String[] {});
        System.out.println("ARGUMENTS:");
        System.out.println("=========");
        for (String arg : args)
            System.out.printf("   %s%n", arg);

        System.out
            .printf("Command line: %s%n", processInfo.commandLine().orElse(""));

        System.out.printf("Start time: %s%n",
            processInfo.startInstant().orElse(Instant.now()).toString());

        System.out.printf("Run time duration: %sms%n",
            processInfo.totalCpuDuration().orElse(Duration.ofMillis(0))
                .toMillis());

        System.out.printf("Owner: %s%n", processInfo.user().orElse(""));

    }

    @Test
    public void listAllLiveProcesses() {
        Stream<ProcessHandle> liveProcesses = ProcessHandle.allProcesses();
        liveProcesses.filter(ProcessHandle::isAlive)
            .filter(ph -> ph.info().command().isPresent())
            .forEach(ph -> {
                System.out.println(ph.pid());
                System.out.println(ph.info().command().get());
            });
    }


    @Test
    public void onExitProcess()
        throws IOException, ExecutionException, InterruptedException {

        ProcessBuilder processBuilder
            = new ProcessBuilder(ProcessUtils.CHESS_APP);
        Process process = processBuilder.start();

        ProcessHandle processHandle = process.toHandle();

        System.out.printf("PID: %s has started %n", processHandle.pid());
        ProcessUtils.echoProcessInfo(processHandle);


        CompletableFuture<ProcessHandle> onProcessExit
            = processHandle.onExit();
        onProcessExit.get();

        onProcessExit.thenAccept(ph -> {
            System.out.printf("%n PID: %s has stopped %n", ph.pid());
            ProcessUtils.echoProcessInfo(ph);
        });
    }

    @Test
    public void killProcess() throws IOException {
        System.out.println("STARTING CHESS");
        System.out.println("==============");
        ProcessBuilder processBuilder
            = new ProcessBuilder(ProcessUtils.CHESS_APP);
        Process chessApp = processBuilder.start();
        ProcessUtils.echoProcessInfo(chessApp.toHandle());

        System.out.println("KILLING CHESS");
        System.out.println("=============");

        ProcessHandle
            .allProcesses()
            .filter(ph -> ph.info().command().isPresent())
            .filter(ph -> ph.info().command().get().contains("Chess"))
            .forEach(process ->
                process.info().command().ifPresent(command ->
                    closeAndLog(process, command)));
    }


    private void closeAndLog(ProcessHandle process, String command)
    {
        String status = process.destroyForcibly() ? " Success!" : " Failed";
        System.out.println("Killing " + command + status);
    }

    @Test
    public void longRunningProcesses() throws IOException {
        final String userName = System.getProperty("user.name");
        System.out.println(userName);
        ProcessHandle
            .allProcesses()
            .map(ProcessHandle::info)
            .sorted(Comparator.comparing(info ->
                info.totalCpuDuration().orElse(Duration.ZERO)))
            .forEach(info ->
                info.command().ifPresent(command ->
                    info.totalCpuDuration().ifPresent(duration ->
                        System.out.println(command + " has been running for " + duration))));

    }


}
