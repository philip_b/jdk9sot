package be.c4j.sot.process;

import org.junit.Test;

import java.io.IOException;


public class ProcessBuilderDemo {

    @Test
    public void startTeminalProcessPreJava5()
        throws IOException {
        System.out.println("START PROCESS:");
        System.out.println("==============");
        Process process = Runtime.getRuntime().exec(ProcessUtils.TERMINAL_APP);
        System.out.println("PROCESS STARTED - ECHO INFORMATION");
        System.out.println("==================================");

        ProcessHandle processHandle = process.toHandle();
        ProcessUtils.echoProcessInfo(processHandle);
    }

    @Test
    public void startTeminalProcess()
        throws IOException {
        System.out.println("START PROCESS:");
        System.out.println("==============");
        Process process = new ProcessBuilder(ProcessUtils.TERMINAL_APP).inheritIO().start();
        System.out.println("PROCESS STARTED - ECHO INFORMATION");
        System.out.println("==================================");

        ProcessHandle processHandle = process.toHandle();
        ProcessUtils.echoProcessInfo(processHandle);
    }

    @Test
    public void startJavaVersionProcess() throws IOException{
        System.out.println("JAVA COMMAND:");
        System.out.println("=============");
        String javaCmd = ProcessUtils.getJavaCmd().getAbsolutePath();
        System.out.println(javaCmd);
        System.out.println("START PROCESS:");
        System.out.println("==============");
        ProcessBuilder processBuilder = new ProcessBuilder(javaCmd, "-version");
        Process process = processBuilder.inheritIO().start();
        //Process process = processBuilder.start();
        System.out.println("PROCESS STARTED - ECHO INFORMATION");
        System.out.println("==================================");

        ProcessHandle processHandle = process.toHandle();
        ProcessUtils.echoProcessInfo(processHandle);
    }
}
