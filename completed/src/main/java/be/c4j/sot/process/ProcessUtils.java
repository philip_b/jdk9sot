package be.c4j.sot.process;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

public class ProcessUtils {

    public static String TERMINAL_APP = "/Applications/Utilities/Terminal.app/Contents/MacOS/Terminal";
    public static String CHESS_APP = "/Applications/Chess.app/Contents/MacOS/Chess";

    public static File getJavaCmd() throws IOException {
        String javaHome = System.getProperty("java.home");
        File javaCommand = new File(javaHome, "bin/java");

        if (javaCommand.canExecute()) {
            return javaCommand;
        } else {
            throw new UnsupportedOperationException(javaCommand.getCanonicalPath() + " is not executable");
        }
    }

    public static void echoProcessInfo(ProcessHandle processHandle){
        System.out.println("PROCESS INFORMATION");
        System.out.println("===================");
        System.out.printf("Process id: %d%n", processHandle.pid());

        ProcessHandle.Info processInfo = processHandle.info();

        System.out.printf("Command: %s%n", processInfo.command().orElse(""));
        //Process Arguments
        String[] args = processInfo.arguments().orElse(new String[] {});
        System.out.println("ARGUMENTS:");
        System.out.println("=========");
        for (String arg : args)
            System.out.printf("   %s%n", arg);

        System.out
            .printf("Command line: %s%n", processInfo.commandLine().orElse(""));

        System.out.printf("Start time: %s%n",
            processInfo.startInstant().orElse(Instant.now()).toString());

        System.out.printf("Run time duration: %sms%n",
            processInfo.totalCpuDuration().orElse(Duration.ofMillis(0))
                .toMillis());

        System.out.printf("Owner: %s%n", processInfo.user().orElse(""));
    }
}

