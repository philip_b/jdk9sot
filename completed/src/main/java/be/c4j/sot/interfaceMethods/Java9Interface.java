package be.c4j.sot.interfaceMethods;

public interface Java9Interface {

    String CONSTANT = "A constant String";

    void abstractMethod(String message);

    default void defaultMethod() {
        log("This is a default method in the interface");
    }

    static void publicStaticDefaultMethod(String message) {
        privateStaticMethod("Public static method in interface");
        privateStaticMethod("Public static method in interface and " + message);
    }

    private void log(String message) {
        System.out.println(message);
    }

    private static void privateStaticMethod(String message) {
        System.out.println("private static method called from: " + message);
    }

}
