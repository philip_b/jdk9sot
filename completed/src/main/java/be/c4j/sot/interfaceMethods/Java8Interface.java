package be.c4j.sot.interfaceMethods;

public interface Java8Interface {

    String CONSTANT = "A constant String";

    void abstractMethod(String message);

    default void defaultMethod() {
        System.out.println("This is a default method in the interface");
    }

    static void publicStaticDefaultMethod(String message) {
        System.out.println("Public static method in interface and via " + message);
    }


}
