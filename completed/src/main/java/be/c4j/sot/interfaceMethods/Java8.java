package be.c4j.sot.interfaceMethods;

public class Java8 implements Java8Interface {

    @Override
    public void abstractMethod(final String message) {
        System.out.println("This method needs to be implemented " + message);
    }

//    @Override
//    public void defaultMethod() {
//        System.out.println("Overriden default method");
//    }

    public void executeMethods(){
        Java8Interface instance = new Java8();
        System.out.println("Constant: " + Java8Interface.CONSTANT);
        //default interface method
        instance.defaultMethod();
        //overriden
        defaultMethod();
        //abstract
        abstractMethod(" in implementation");
        //Static interface method
        Java8Interface.publicStaticDefaultMethod("Static method executed from implementation");
    }
}
