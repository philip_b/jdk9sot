package be.c4j.sot.interfaceMethods;


public interface Java7Interface {
    String CONSTANT = "A constant String";

    void abstractMethod(String message);

}
