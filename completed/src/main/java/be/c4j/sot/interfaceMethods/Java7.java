package be.c4j.sot.interfaceMethods;


public class Java7 implements Java7Interface {

    @Override
    public void abstractMethod(final String message) {
        System.out.println("This method needs to be implemented " + message);
    }

    public void executeMethods(){
        Java7Interface instance = new Java7();
        System.out.println("Constant: " + Java7Interface.CONSTANT);
        //abstract
        abstractMethod(" in implementation");
    }
}
