package be.c4j.sot.interfaceMethods;

public class Java9 implements Java9Interface {

    @Override
    public void abstractMethod(final String message) {
        System.out.println("This method needs to be implemented." + message);
    }

    @Override
    public void defaultMethod() {
        System.out.println("This method is overriden.");
    }


    public void executeMethods(){
        Java9Interface instance = new Java9();
        System.out.println("Constant: " + Java9Interface.CONSTANT);
        //default interface method
        instance.defaultMethod();
        //overriden
        defaultMethod();
        //abstract
        abstractMethod(" from Implementation");
        //Static interface method
        Java9Interface.publicStaticDefaultMethod("Static method executed from implementation");
    }
}
