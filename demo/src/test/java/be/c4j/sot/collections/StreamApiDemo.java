package be.c4j.sot.collections;

import org.junit.Test;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class StreamApiDemo {

    @Test
    public void forLoop() throws Exception {
        ValueHolder a = new ValueHolder(0);
        for (int i = 0; i < 10; i++) {
            a.add(i);
        }

        assertThat(a.getValue()).isEqualTo(45);
    }


    @Test
    public void forLoop_enkelEvenGetallen() throws Exception {
        ValueHolder a = new ValueHolder(0);
        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0)
                a.add(i);
        }

        assertThat(a.getValue()).isEqualTo(20);
    }


    @Test
    public void forLoop_NegeerDeEerste5Getallen() throws Exception {
        ValueHolder a = new ValueHolder(0);
        for (int i = 0; i < 10; i++) {
            if (i >= 5)
                a.add(i);
        }

        assertThat(a.getValue()).isEqualTo(35);
    }


    @Test
    public void nullableStream() throws Exception {
        StreamHolder h = apiCallThatReturnsNull();

        Stream<String> versies = null == h ? Stream.empty() : h.getVersies();

        assertThat(versies.collect(Collectors.toList())).isEmpty();


        StreamHolder withValues = apiCallThatReturnsValues("Java 7", "Java 8", "Java 9");
        Stream<String> bestaandeVersies = Optional.ofNullable(withValues)
                .map(StreamHolder::getVersies)
                .orElse(Stream.empty());
        assertThat(bestaandeVersies.collect(Collectors.toList())).contains("Java 7", "Java 8", "Java 9");

    }

    private StreamHolder apiCallThatReturnsValues(String... versies) {
        return new StreamHolder(versies);
    }

    private StreamHolder apiCallThatReturnsNull() {
        return null;
    }

    private class StreamHolder {
        private Stream<String> versies;

        public StreamHolder(String... versies) {
            this.versies = Stream.of(versies);
        }

        public Stream<String> getVersies() {
            return versies;
        }
    }

    private class ValueHolder {
        private int value;

        public ValueHolder(int value) {
            this.value = value;
        }


        public void add(Integer toAdd) {
            value = value + toAdd;
        }

        public int getValue() {
            return value;
        }
    }
}
