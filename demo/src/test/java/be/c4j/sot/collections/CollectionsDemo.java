package be.c4j.sot.collections;

import org.junit.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

public class CollectionsDemo {



    @Test
    public void arrrayList_Manual() throws Exception {
        List<String> values = new ArrayList<>();
        values.add("Java 7");
        values.add("Java 8");
        values.add("Java 9");

        assertThat(values).contains("Java 7","Java 8","Java 9");

    }

    @Test
    public void set_Manual() throws Exception {
        Set<String> values = new HashSet<>();
        values.add("Java 7");
        values.add("Java 7");
        values.add("Java 8");
        values.add("Java 9");

        assertThat(values).contains("Java 7","Java 8","Java 9").hasSize(3);
    }

    @Test
    public void map_Manual() throws Exception {
        Map<String,Integer> versions = new HashMap<>();
        versions.put("Java 7",2011);
        versions.put("Java 8",2014);
        versions.put("Java 9",2017);

        assertThat(versions).contains(entry("Java 7",2011),entry("Java 8",2014),entry("Java 9",2017));
    }
}
