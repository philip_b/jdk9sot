package be.c4j.sot.interfaceMethods;

import org.junit.Test;

public class InterfaceDemo {

    @Test
    public void executeJava7(){
        Java7 java7 = new Java7();
        java7.executeMethods();
    }

    @Test
    public void executeJava8(){
        Java8 java8 = new Java8();
        java8.executeMethods();
    }

    @Test
    public void executeJava9(){
        Java9 java9 = new Java9();
        java9.executeMethods();
    }

}
