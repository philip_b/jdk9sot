package be.c4j.sot.process;

import org.junit.Test;

import java.io.IOException;

public class ProcessBuilderDemo {

    @Test
    public void startTeminalProcessPreJava5()
        throws IOException {
        System.out.println("START PROCESS:");
        System.out.println("==============");

        System.out.println("PROCESS STARTED - ECHO INFORMATION");
        System.out.println("==================================");


    }

    @Test
    public void startTeminalProcess()
        throws IOException {
        System.out.println("START PROCESS:");
        System.out.println("==============");

        System.out.println("PROCESS STARTED - ECHO INFORMATION");
        System.out.println("==================================");


    }

    @Test
    public void startJavaVersionProcess() throws IOException{
        System.out.println("JAVA COMMAND:");
        System.out.println("=============");


        System.out.println("START PROCESS:");
        System.out.println("==============");

        //Process process = processBuilder.start();
        System.out.println("PROCESS STARTED - ECHO INFORMATION");
        System.out.println("==================================");


    }
}
