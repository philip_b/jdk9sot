package be.c4j.sot.process;

import org.junit.Test;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Comparator;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

public class ProcessHandlerDemo {

    @Test
    public void findSpecificProcess() throws IOException {



    }

    @Test
    public void currentProcess() throws IOException {


        System.out.println("PROCESS INFORMATION");
        System.out.println("===================");


    }

    @Test
    public void listAllLiveProcesses() {

    }


    @Test
    public void onExitProcess()
        throws IOException, ExecutionException, InterruptedException {


    }

    @Test
    public void killProcess() throws IOException {

    }


    private void closeAndLog(ProcessHandle process, String command)
    {
        String status = process.destroyForcibly() ? " Success!" : " Failed";
        System.out.println("Killing " + command + status);
    }

    @Test
    public void longRunningProcesses() throws IOException {
        final String userName = System.getProperty("user.name");
        System.out.println(userName);
        ProcessHandle
            .allProcesses()
            .map(ProcessHandle::info)
            .sorted(Comparator.comparing(info ->
                info.totalCpuDuration().orElse(Duration.ZERO)))
            .forEach(info ->
                info.command().ifPresent(command ->
                    info.totalCpuDuration().ifPresent(duration ->
                        System.out.println(command + " has been running for " + duration))));

    }


}
